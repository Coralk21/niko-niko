import { inject } from '@angular/core';
import { CanActivateFn, Router } from '@angular/router';
import { SessionStorageService } from '../services/session-storage.service';
import { StorageKey } from '../constants/storage-key.constant';

export const privateGuard: CanActivateFn = (route, state) => {
  const storageService = inject(SessionStorageService);
  const router = inject(Router);

  if (!storageService.getItem(StorageKey.TOKEN)) {
    router.navigate(['/auth/login']);
    return false;
  }

  return true;
};