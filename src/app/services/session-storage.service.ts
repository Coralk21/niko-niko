import { Injectable } from '@angular/core';
import { StorageKey } from '../constants/storage-key.constant';

@Injectable({
  providedIn: 'root',
})
export class SessionStorageService {
  constructor() {}

  setItem(item: StorageKey, value: string | object): void {
    if (typeof value !== 'string') value = JSON.stringify(value);
    sessionStorage.setItem(this.toBase64(item), this.toBase64(value));
  }

  getItem<T>(item: StorageKey): T | null {
    let value = sessionStorage.getItem(this.toBase64(item));
    if (value) {
      try {
        return JSON.parse(this.toUtf8(value));
      } catch {}
      return this.toUtf8(value) as T;
    }
    return null;
  }

  removeItem(item: StorageKey): void {
    sessionStorage.removeItem(this.toBase64(item));
  }

  toBase64(value: string): string {
    return btoa(value);
  }

  toUtf8(value: string): string {
    return atob(value);
  }
}
