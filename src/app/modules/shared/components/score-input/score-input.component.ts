import { Component } from '@angular/core';
import {
  AbstractControl,
  ControlValueAccessor,
  NG_VALIDATORS,
  NG_VALUE_ACCESSOR,
  ValidationErrors,
  Validator,
} from '@angular/forms';

@Component({
  selector: 'app-score-input',
  templateUrl: './score-input.component.html',
  styleUrls: ['./score-input.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      multi: true,
      useExisting: ScoreInputComponent,
    },
    {
      provide: NG_VALIDATORS,
      multi: true,
      useExisting: ScoreInputComponent,
    },
  ],
})
export class ScoreInputComponent implements ControlValueAccessor, Validator {
  protected scores = [
    {
      value: 1,
      color: '#E35886',
    },
    {
      value: 2,
      color: '#E2CE1D',
    },
    {
      value: 3,
      color: '#56B1CE',
    },
    {
      value: 4,
      color: '#72D0B6',
    },
  ];
  score = 0;

  onChange = (score: Number) => {};
  onTouched = () => {};
  touched = false;
  disabled = false;

  updateScore(value: number) {
    this.markAsTouched();
    if (!this.disabled) {
      if (this.score && this.score === value) {
        this.score = 0;
      } else {
        this.score = value;
      }
      this.onChange(this.score);
    }
  }

  markAsTouched() {
    if (!this.touched) {
      this.onTouched();
      this.touched = true;
    }
  }

  writeValue(score: number): void {
    this.score = score;
  }

  registerOnChange(fn: any): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }

  setDisabledState(isDisabled: boolean): void {
    this.disabled = isDisabled;
  }

  validate(control: AbstractControl): ValidationErrors | null {
    const score = control.value;
    if (score < 1 || score > 4) {
      return {
        outOfRange: {
          score,
        },
      };
    }
    return null;
  }
}
