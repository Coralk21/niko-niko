import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ScoreInputComponent } from './components/score-input/score-input.component';

@NgModule({
  declarations: [ScoreInputComponent],
  imports: [CommonModule],
  exports: [ScoreInputComponent],
})
export class SharedModule {}
