import { Component } from '@angular/core';
import { NonNullableFormBuilder, Validators } from '@angular/forms';
import { AuthService } from '../../services/auth.service';
import { NzMessageService } from 'ng-zorro-antd/message';
import { HttpErrorResponse } from '@angular/common/http';
import { Router } from '@angular/router';
import { SessionStorageService } from 'src/app/services/session-storage.service';
import { StorageKey } from 'src/app/constants/storage-key.constant';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent {
  loginForm = this.fb.group({
    username: ['', Validators.required],
    password: ['', Validators.required],
  });

  constructor(
    private fb: NonNullableFormBuilder,
    private authService: AuthService,
    private messageService: NzMessageService,
    private router: Router,
    private sessionStorage: SessionStorageService
  ) {}

  login() {
    if (this.loginForm.valid) {
      this.authService.login(this.loginForm.getRawValue()).subscribe({
        next: (token) => {
          this.sessionStorage.setItem(StorageKey.TOKEN, token);
          this.messageService.success('Inicio de sesión exitoso.');
          this.router.navigate(['/dashboard']);
        },
        error: (error) => {
          if (error instanceof HttpErrorResponse && error.status === 401) {
            this.messageService.error('Usuario y/o contraseña no válidos.');
          } else {
            this.messageService.error(
              'Ha ocurrido un error. Intenta más tarde.'
            );
          }
        },
      });
    } else {
      Object.keys(this.loginForm.controls).forEach((key) => {
        this.loginForm.get(key)?.markAsDirty();
        this.loginForm.get(key)?.updateValueAndValidity({ onlySelf: true });
      });
    }
  }
}
