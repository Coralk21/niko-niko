import { Component } from '@angular/core';
import {
  FormBuilder,
  NonNullableFormBuilder,
  Validators,
} from '@angular/forms';
import { AuthService } from '../../services/auth.service';
import { HttpErrorResponse } from '@angular/common/http';
import { NzMessageService } from 'ng-zorro-antd/message';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss'],
})
export class RegisterComponent {
  registerForm = this.fb.group({
    name: ['', Validators.required],
    username: [
      '',
      [Validators.required, Validators.pattern(new RegExp('^[A-Za-z0-9]+$'))],
    ],
    password: ['', Validators.required],
  });

  constructor(
    private fb: NonNullableFormBuilder,
    private authService: AuthService,
    private messageService: NzMessageService,
    private router: Router
  ) {}

  register() {
    if (this.registerForm.valid) {
      this.authService.registerUser(this.registerForm.getRawValue()).subscribe({
        next: () => {
          this.messageService.success('Usuario creado exitosamente.');
          this.registerForm.reset();
          this.router.navigate(['/auth/login']);
        },
        error: (error) => {
          if (error instanceof HttpErrorResponse && error.status === 409) {
            this.messageService.error(
              'El nombre de usuario no está disponible.'
            );
          } else {
            this.messageService.error(
              'Ha ocurrido un error, intenta más tarde.'
            );
          }
        },
      });
    } else {
      Object.keys(this.registerForm.controls).forEach((key) => {
        this.registerForm.get(key)?.markAsDirty();
        this.registerForm.get(key)?.updateValueAndValidity({ onlySelf: true });
      });
    }
  }
}
