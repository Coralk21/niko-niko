import { Injectable } from '@angular/core';
import { NewUserDto } from '../dtos/new-user.dto';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { ApiUri } from 'src/app/constants/api-uri.constant';
import { UserCredentialsDto } from '../dtos/user-credentials.dto';
import { TokenDataDto } from '../dtos/token-data.dto';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  constructor(private http: HttpClient) {}

  registerUser(data: NewUserDto): Observable<{ id: number }> {
    return this.http.post<{ id: number }>(ApiUri.REGISTER, data);
  }

  login(credentials: UserCredentialsDto): Observable<TokenDataDto> {
    return this.http.post<TokenDataDto>(ApiUri.LOGIN, credentials);
  }
}
