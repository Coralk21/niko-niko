export interface NewUserDto {
    name: string;
    username: string;
    password: string;
}