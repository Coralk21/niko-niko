export interface TokenDataDto {
  type: string;
  token: string;
}
