import { AfterViewInit, Component, ElementRef, ViewChild } from '@angular/core';
import { ReportPeriodicity } from './constants/report-periodicity.constant';
import {
  addDays,
  addMonths,
  endOfMonth,
  endOfWeek,
  format,
  startOfMonth,
  startOfWeek,
  subDays,
  subMonths,
} from 'date-fns';
import { ScoreService } from '../dashboard/services/score.service';
import { Chart, ChartData } from 'chart.js/auto';
import { Score } from 'src/app/models/score.model';

@Component({
  selector: 'app-reports',
  templateUrl: './reports.component.html',
  styleUrls: ['./reports.component.scss'],
})
export class ReportsComponent implements AfterViewInit {
  @ViewChild('chart') chartView!: ElementRef<HTMLCanvasElement>;

  periodicity: ReportPeriodicity = ReportPeriodicity.MONTHLY;
  base = new Date();
  chart?: Chart;
  color = '#1890ff';

  get range(): string {
    return this.periodicity === ReportPeriodicity.MONTHLY
      ? `${this.getMonthLimits().start} - ${this.getMonthLimits().end}`
      : `${this.getWeekLimits().start} - ${this.getWeekLimits().end}`;
  }

  constructor(private scoreService: ScoreService) {}

  ngAfterViewInit(): void {
    this.loadData();
  }

  loadData() {
    const limits =
      this.periodicity === ReportPeriodicity.MONTHLY
        ? this.getMonthLimits()
        : this.getWeekLimits();

    this.scoreService.getByDates(limits.start, limits.end).subscribe({
      next: (data) => {
        this.updateChart(this.getChartData(data));
      },
      error: (error) => console.log(error),
    });
  }

  updateChart(data: ChartData) {
    if (!this.chart) {
      this.chart = new Chart(this.chartView.nativeElement, {
        type: 'line',
        options: {
          responsive: true,
          backgroundColor: this.color,
          interaction: {
            intersect: false,
          },
          scales: {
            y: {
              suggestedMin: 1,
              ticks: {
                stepSize: 1,
              },
            },
          },
        },
        data,
      });
    } else {
      this.chart.data = data;
      this.chart.update();
    }
  }

  getChartData(scores: Score[]): ChartData {
    return {
      labels: scores.map((score) => format(new Date(score.created_at), 'dd')),
      datasets: [
        {
          label: 'Niko Niko',
          data: scores.map((score) => score.score),
          borderColor: this.color,
          fill: false,
        },
      ],
    };
  }

  getWeekLimits() {
    const start = format(startOfWeek(this.base), 'yyyy-MM-dd');
    const end = format(endOfWeek(this.base), 'yyyy-MM-dd');
    return { start, end };
  }

  getMonthLimits() {
    const start = format(startOfMonth(this.base), 'yyyy-MM-dd');
    const end = format(endOfMonth(this.base), 'yyyy-MM-dd');
    return { start, end };
  }

  setMonthly() {
    this.periodicity = ReportPeriodicity.MONTHLY;
    this.loadData();
  }

  setWeekly() {
    this.periodicity = ReportPeriodicity.WEEKLY;
    this.loadData();
  }

  previous() {
    this.base =
      this.periodicity === ReportPeriodicity.MONTHLY
        ? subMonths(this.base, 1)
        : subDays(this.base, 7);
    this.loadData();
  }

  next() {
    this.base =
      this.periodicity === ReportPeriodicity.MONTHLY
        ? addMonths(this.base, 1)
        : addDays(this.base, 7);
    this.loadData();
  }
}
