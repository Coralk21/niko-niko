import { Component } from '@angular/core';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.scss'],
})
export class NavComponent {
  menu = [
    {
      label: 'Hoy',
      route: '/dashboard/today',
    },
    {
      label: 'Reportes',
      route: '/dashboard/reports',
    },
  ];
}
