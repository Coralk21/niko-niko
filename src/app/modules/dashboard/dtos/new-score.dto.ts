export interface NewScoreDto {
  score: number;
  comment: string;
}
