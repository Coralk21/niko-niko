import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { NewScoreDto } from '../dtos/new-score.dto';
import { Score } from 'src/app/models/score.model';
import { ApiUri } from 'src/app/constants/api-uri.constant';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class ScoreService {
  constructor(private http: HttpClient) {}

  saveScore(score: NewScoreDto): Observable<Score> {
    return this.http.post<Score>(ApiUri.SCORES, score);
  }

  getByDates(start: string, end: string): Observable<Score[]> {
    return this.http.get<Score[]>(ApiUri.SCORES, {
      params: new HttpParams().set('startDate', start).set('endDate', end),
    });
  }

  getCurrent(): Observable<Score | null> {
    return this.http.get<Score | null>(ApiUri.CURRENT_SCORE);
  }
}
