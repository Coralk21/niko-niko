import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DashboardRoutingModule } from './dashboard-routing.module';
import { DashboardComponent } from './dashboard.component';
import { NavComponent } from './components/nav/nav.component';
import { NzCardModule } from 'ng-zorro-antd/card';

@NgModule({
  declarations: [DashboardComponent, NavComponent],
  imports: [CommonModule, DashboardRoutingModule, NzCardModule],
})
export class DashboardModule {}
