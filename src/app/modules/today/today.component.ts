import { Component, OnInit } from '@angular/core';
import { FormGroup, NonNullableFormBuilder, Validators } from '@angular/forms';
import { ScoreService } from '../dashboard/services/score.service';
import { NzMessageService } from 'ng-zorro-antd/message';
import { Score } from 'src/app/models/score.model';

@Component({
  selector: 'app-today',
  templateUrl: './today.component.html',
  styleUrls: ['./today.component.scss'],
})
export class TodayComponent implements OnInit {
  todayForm: FormGroup;
  filled = false;

  constructor(
    private fb: NonNullableFormBuilder,
    private scoreService: ScoreService,
    private messageService: NzMessageService
  ) {
    this.todayForm = this.buildForm();
  }

  ngOnInit() {
    this.getCurrent();
  }

  buildForm(score?: Score) {
    return this.fb.group({
      score: [score?.score || 0, Validators.required],
      comment: [score?.comment || ''],
    });
  }

  getCurrent() {
    this.scoreService.getCurrent().subscribe({
      next: (score) => {
        if (score) {
          this.todayForm = this.buildForm(score);
          this.filled = true;
          this.todayForm.disable();
        }
      },
      error: (error) => console.log(error),
    });
  }

  save() {
    if (this.todayForm.valid) {
      this.scoreService.saveScore(this.todayForm.getRawValue()).subscribe({
        next: () => {
          this.messageService.success('Datos guardados exitosamente.');
          this.todayForm.disable();
          this.filled = true;
        },
        error: () => {
          this.messageService.error('Ha ocurrido un error. Intenta mas tarde.');
        },
      });
    } else {
      Object.keys(this.todayForm.controls).forEach((key) => {
        this.todayForm.get(key)?.markAsDirty();
        this.todayForm.get(key)?.updateValueAndValidity({ onlySelf: true });
      });
    }
  }
}
