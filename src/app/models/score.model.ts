export interface Score {
  id: number;
  score: number;
  comment: string;
  created_at: string;
}
