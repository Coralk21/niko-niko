import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
} from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { SessionStorageService } from '../services/session-storage.service';
import { StorageKey } from '../constants/storage-key.constant';
import { TokenDataDto } from '../modules/auth/dtos/token-data.dto';
import { Router } from '@angular/router';
import { publicApi } from '../constants/public-api.constant';

@Injectable()
export class ApiInterceptor implements HttpInterceptor {
  constructor(
    private sessionStorage: SessionStorageService,
    private router: Router
  ) {}

  setBaseUrl(request: HttpRequest<unknown>): HttpRequest<unknown> {
    return request.clone({
      url: environment.apiBaseUrl + request.url,
    });
  }

  setToken(request: HttpRequest<unknown>): HttpRequest<unknown> {
    if (publicApi.includes(request.url)) return request;
    const token = this.sessionStorage.getItem<TokenDataDto>(StorageKey.TOKEN);
    if (!token) {
      this.router.navigate(['/auth/login']);
      throw new Error('Unauthorized');
    }
    return request.clone({
      headers: request.headers.append(
        'Authorization',
        `${token.type} ${token.token}`
      ),
    });
  }

  intercept(
    request: HttpRequest<unknown>,
    next: HttpHandler
  ): Observable<HttpEvent<unknown>> {
    return next.handle(this.setBaseUrl(this.setToken(request)));
  }
}
