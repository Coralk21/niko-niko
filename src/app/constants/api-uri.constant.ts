export class ApiUri {
  static readonly REGISTER = '/auth/register';
  static readonly LOGIN = '/auth/login';
  static readonly SCORES = '/scores';
  static readonly CURRENT_SCORE = `${ApiUri.SCORES}/current`;
}
