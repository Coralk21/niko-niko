import { ApiUri } from './api-uri.constant';

export const publicApi = [ApiUri.LOGIN, ApiUri.REGISTER];
